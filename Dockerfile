FROM frolvlad/alpine-glibc:alpine-3.9_glibc-2.28 AS builder

ARG VERSION=0.2.2

WORKDIR /var/tmp

RUN apk add curl && \
    curl -O -L "https://github.com/mcuadros/ofelia/releases/download/v${VERSION}/ofelia_v${VERSION}_linux_amd64.tar.gz" && \
    tar -xvzf ofelia*.tar.gz --strip-components=1

# Simple test of binary file
RUN ./ofelia | grep ofelia && \
    ./ofelia | grep ${VERSION}

################
# TARGET IMAGE #
################

FROM frolvlad/alpine-glibc:alpine-3.9_glibc-2.28
# FROM golang:1.10.0

LABEL maintainer="Andrey Mukhin <andrey@nufl.ru>"

COPY --from=builder /var/tmp/ofelia /usr/bin/ofelia

ENTRYPOINT ["/usr/bin/ofelia"]
CMD ["daemon", "--config", "/etc/ofelia/config.ini"]
